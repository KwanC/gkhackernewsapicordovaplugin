//
//  GKHackerNewsApiPlugin.m
//  KMCCordovaApp
//
//  Created by Kwan Cheng on 2/25/17.
//
//

#import "GKHackerNewsApiPlugin.h"

@interface GKHackerNewsApiPlugin() {
    GKHackerNews* _gkhn;
    NSString* callbackId;
}
@end

@implementation GKHackerNewsApiPlugin
-(void) initSDK:(CDVInvokedUrlCommand*)command {
    _gkhn = [[GKHackerNews alloc]init];
    _gkhn.delegate = self;
    [_gkhn initSDK];

    callbackId = command.callbackId;
}

-(void) showHackerNews:(CDVInvokedUrlCommand*)command {
    [_gkhn showHackerNews];
}
    
-(void) topStoriesListAvailable {
    if(callbackId) {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [result setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    }
}
@end
